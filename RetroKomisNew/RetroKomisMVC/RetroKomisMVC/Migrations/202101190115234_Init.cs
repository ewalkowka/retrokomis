﻿namespace RetroKomisMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Car",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(unicode: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Registration = c.String(unicode: false),
                        Milometer = c.Int(nullable: false),
                        FirstRegistration = c.Int(nullable: false),
                        Description = c.String(unicode: false),
                        FotoUrl = c.String(unicode: false),
                        MiniFotoUrl = c.String(unicode: false),
                        OwnerId = c.Int(nullable: false),
                        Brand = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Email = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Content = c.String(nullable: false, maxLength: 1000, storeType: "nvarchar"),
                        AnswerMe = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        Password = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        Name = c.String(nullable: false, unicode: false),
                        Surname = c.String(nullable: false, unicode: false),
                        Email = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        UserRole = c.Int(nullable: false),
                        DrivingLicenseNumber = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.User");
            DropTable("dbo.Comment");
            DropTable("dbo.Car");
        }
    }
}
