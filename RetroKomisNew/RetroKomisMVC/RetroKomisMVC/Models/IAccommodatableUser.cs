﻿namespace RetroKomisMVC.Models
{
    public interface IAccommodatableUser
    {
        string Login { get; set; }
        void AccommodateUser(string user);
    }
}
