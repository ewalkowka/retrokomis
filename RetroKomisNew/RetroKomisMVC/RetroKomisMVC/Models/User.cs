﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.ModelBinding;

namespace RetroKomisMVC.Models
{
    public class User : ILogger<User>, IAccommodatableUser
    {

        [BindNever]
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Login { get; set; }
        [Required, StringLength(50)]
        public string Password { get; private set; }
        [Required(ErrorMessage = "Imię jest wymagane")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Adres email jest wymagany")]
        [StringLength(100, ErrorMessage = "Adres email jest zbyt długi")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
   ErrorMessage = "Niepoprawny adres email")]
        public string Email { get; set; }
        [Required]
        public int PhoneNumber;

        public enum ERole
        {
            Guest,
            User,
            Admin
        }

        public ERole UserRole { get; set; } = ERole.User;

        [Required, StringLength(50)]
        public string DrivingLicenseNumber { get; set; }

        public static User CurrentUser;

        public IEnumerable<Car> GetMyCars(int userId, DbSet<Car> database)
        {
            List<Car> myCars = new List<Car>();

            for (int i = 0; i < database.ToList().Count; i++)
            {
                Car currentCar = database.ToList()[i];

                if (userId == currentCar.OwnerId) myCars.Add(currentCar);
            }

            return myCars;
        }

        public static bool Verified(string email, string password, DbSet<User> database)
        {
            foreach (var user in database)
            {
                if (user.Password == password && user.Email == email)
                {
                    CurrentUser = user;
                    return true;
                }
            }

            return false;
        }

        void IAccommodatableUser.AccommodateUser(string user)
        {
            Regex checkCarRegex = new Regex(@"(?<Login>\D+)\b", RegexOptions.Compiled);


            var matches = checkCarRegex.Matches(user);

            foreach (Match match in matches)
            {
                var groups = match.Groups;

                Login = groups["Login"].Value;
            }
        }
    }
}