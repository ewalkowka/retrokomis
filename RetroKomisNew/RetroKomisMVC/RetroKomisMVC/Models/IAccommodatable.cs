﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetroKomisMVC.Models
{
    public interface IAccommodatable
    {
            string Brand { get; set; }
            string Model { get; set; }
            decimal Price { get; set; }
            int Milometer { get; set; }
            string Registration { get; set; }
            void Accommodate(string car);
        
    }
}
