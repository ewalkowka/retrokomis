﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace RetroKomisMVC.Models
{
    /// <summary>
    /// Klasa komentarza dodawanego pod danymy ogłoszeniami
    /// </summary>
    public class Comment
    {
        [BindNever]
        public int Id { get; set; }

        [Required(ErrorMessage = "Pole wymagane")]
        [StringLength(100, ErrorMessage = "Nazwa użytkownika jest za długa")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Adres email jest wymagany")]
        [StringLength(100, ErrorMessage = "Adres email jest zbyt długi")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
           ErrorMessage = "Niepoprawny adres email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Treść jest wymagana")]
        [StringLength(1000)]
        public string Content { get; set; }

        public bool AnswerMe { get; set; }

        public static List<Comment> Comments = new List<Comment>();

        public Comment() { }

        public static List<Comment> GetComments()
        {
            return Comments;
        }

        public void AddComment(Comment comment)
        {
            Comments.Add(comment);
        }
    }
}
