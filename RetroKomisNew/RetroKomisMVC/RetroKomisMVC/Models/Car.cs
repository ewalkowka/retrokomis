﻿using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.ModelBinding;

namespace RetroKomisMVC.Models
{
    /// <summary>
    /// Klasa samochodu
    /// </summary>
    public class Car : IAccommodatable
    {
        #region Properties
        [BindNever]
        public int Id { get; private set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public string Registration { get; set; }
        public int Milometer { get; set; }
        public int FirstRegistration { get; set; }
        public string Description { get; set; }
        public string FotoUrl { get; set; }
        public string MiniFotoUrl { get; set; }
        public int OwnerId { get; set; }

        public string Brand { get; set; }
        #endregion


        public Car() { }

        #region Validating functions

        /// <summary>
        /// Metoda sprawdza czy w bazie znajduje się już samochód o danym numerze rejestracyjnym
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        public static bool CarExistsInContext(Car car, DbSet<Car> database)
        {
            return database.Any(c => c.Registration == car.Registration);
        }

        /// <summary>
        /// Metoda sprawdzająca czy dane pole klasy samochód nie zawiera pustych pól
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        public static bool IsValid(Car car)
        {
            if (string.IsNullOrWhiteSpace(car.Brand.ToString())) return false;
            else if (string.IsNullOrWhiteSpace(car.Model)) return false;
            else if (string.IsNullOrWhiteSpace(car.Registration)) return false;
            else if (!int.TryParse(car.Milometer.ToString(), out _)) return false;
            else if (string.IsNullOrWhiteSpace(car.FirstRegistration.ToString())) return false;
            else if (string.IsNullOrWhiteSpace(car.Description)) return false;
            else return true;
        }

        /// <summary>
        /// Metoda sprawdzająca czy dany string zawiera cyfrę
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool IsStringDigit(string s)
        {
            foreach (char letter in s)
            {
                if (char.IsDigit(letter)) return true;
            }
            return false;
        }

        #endregion

        public void Accommodate(string car)
        {
            Regex checkCarRegex = new Regex(@"(?<Brand>\D+)\b", RegexOptions.Compiled);


            var matches = checkCarRegex.Matches(car);

            foreach (Match match in matches)
            {
                var groups = match.Groups;

                Brand = groups["Brand"].Value;
                Model = groups["Model"].Value;
                if (decimal.TryParse(groups["Price"].Value, out decimal _price)) Price = _price;
                if (int.TryParse(groups["Milometer"].Value, out int _milometer)) Milometer = _milometer;
                Registration = groups["Registration"].Value;
            }
        }
    }
}