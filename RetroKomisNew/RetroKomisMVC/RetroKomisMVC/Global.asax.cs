using AutoMapper;
using log4net;
using log4net.Config;
using RetroKomisMVC.Properties;
using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RetroKomisMVC
{
    public class MvcApplication : System.Web.HttpApplication, ILogger<MvcApplication>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            Stream log4netconfig_stream = new MemoryStream(Resources.log4net);
            XmlConfigurator.Configure(log4netconfig_stream);

            this.GetLogger().Info("Application started");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
