﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RetroKomisMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Edit",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Car", action = "Edit", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Remove",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Car", action = "Remove", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Details",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Car", action = "Details", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "RemovingConfirmed",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Car", action = "RemovingConfirmed", id = UrlParameter.Optional }
                );
        }
    }
}
