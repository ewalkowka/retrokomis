﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetroKomisMVC
{
    public static class Extensions
    {
        public static ILog GetLogger<T>(this ILogger<T> logger)
        {
            return LoggerImpl<T>.Log;
        }
    }
}