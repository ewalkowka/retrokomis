﻿using RetroKomisMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RetroKomisMVC.Controllers
{
    public class LoginController : Controller, ILogger<LoginController>
    {
        // GET: Login

        private readonly ApplicationDbContext context;

        public LoginController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PerformLogin(string email, string password)
        {
            if (RetroKomisMVC.Models.User.Verified(email, password, context.Users))
            {
                Session["Login"] = email;

                this.GetLogger().Info("User " + email + " logged in");

                return RedirectToAction("Dashboard", "Car");
            }
            else return RedirectToAction("Index", "Car");
        }

        public ActionResult Logout()
        {
            Session.Remove("Login");
            return RedirectToAction("Index", "Home");
        }
    }
}