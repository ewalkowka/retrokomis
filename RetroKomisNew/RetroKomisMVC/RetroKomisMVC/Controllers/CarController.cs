using RetroKomisMVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static RetroKomisMVC.Models.User;

namespace RetroKomisMVC.Controllers
{
    public class CarController : Controller, ILogger<CarController>
    {
        private readonly ApplicationDbContext context;

        public CarController()
        {
            context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }

        public ActionResult Index()
        {
            this.GetLogger().Debug("Homepage accessed");

            var cars = context.Cars.ToList();

            if (cars != null) return View(cars);

            else return RedirectToAction("NotFoundEx");
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Car car)
        {
            car.OwnerId = CurrentUser.Id;

            if (car == null) return RedirectToAction("NotFoundEx");
            context.Cars.Add(car);
            context.SaveChanges();
            return RedirectToAction("Index", "Car");
        }
        public ActionResult Dashboard()
        {
            User user = CurrentUser;
            if (CurrentUser == null) return RedirectToAction("NotFoundEx");

            if (user.UserRole == ERole.Admin)
            {
                this.GetLogger().Debug("Admin on board!");
                return View(context.Cars.ToList());
            }
            else if (user.UserRole == ERole.User) return View(user.GetMyCars(user.Id, context.Cars));
            else return RedirectToAction("Index", "Car");
        }

        public ActionResult Edit(int id)
        {
            Car car = context.Cars.SingleOrDefault(c => c.Id == id);

            if (car == null)
                return RedirectToAction("NotFoundEx");

            return View(car);
        }

        [HttpPost]
        public ActionResult PerformEdit(Car car)
        {
            if (context.Cars.Any(c => c.Id == car.Id)) context.Cars.Remove(context.Cars.SingleOrDefault(c => c.Id == car.Id));
            
            context.Cars.Add(car);
            context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Remove(int id)
        {
            Car carToRemove = context.Cars.SingleOrDefault(c => c.Id == id);
            return View(carToRemove);
        }


        [HttpPost]
        public ActionResult RemovingConfirmed(Car car)
        {
            if ((car == null) || (CurrentUser.UserRole != ERole.Admin))
                return RedirectToAction("NotFoundEx");

            if (context.Cars.Any(c => c.Id == car.Id)) context.Cars.Remove(context.Cars.SingleOrDefault(c => c.Id == car.Id));
            context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        public ActionResult NotFoundEx()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            var carToDisplay = context.Cars.SingleOrDefault(c => c.Id == id);

            this.GetLogger().Info("Car " + id + " displayed");

            return View(carToDisplay);
        }

        [HttpPost, ActionName("Details")]
        public ActionResult DisplayDetails(int carId)
        {
            var carToDisplay = context.Cars.SingleOrDefault(c => c.Id == carId);

            if (carToDisplay == null)
                return RedirectToAction("NotFoundEx");

            return View(carToDisplay);
        }

        public ActionResult ShowCar()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchQuery(string query)
        {
            List<Car> queryCars = new List<Car>();

            foreach (Car car in context.Cars)
            {
                if (car.Brand.Contains(query) || car.Model.Contains(query)) queryCars.Add(car);
            }

            return View(queryCars);
        }
    }
}
