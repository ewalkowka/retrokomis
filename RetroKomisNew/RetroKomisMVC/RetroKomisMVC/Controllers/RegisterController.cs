﻿using RetroKomisMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RetroKomisMVC.Controllers
{
    public class RegisterController : Controller, ILogger<CarController>
    {
        private readonly ApplicationDbContext context;

        public RegisterController()
        {
            context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }
        // GET: Register

        /*
        public ActionResult Index()
        {
            return View();
        }
        */

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PerformRegister(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
            this.GetLogger().Info("User " + user.Login + " registered");
            return RedirectToAction("Index", "Car");
        }
    }
}