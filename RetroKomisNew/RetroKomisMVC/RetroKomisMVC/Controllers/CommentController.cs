﻿using RetroKomisMVC;
using RetroKomisMVC.Models;
using System.Web.Mvc;

namespace RetroKomis.Controllers
{
    public class CommentController : Controller, ILogger<CommentController>
    {
        private readonly ApplicationDbContext context;


        public CommentController()
        {
            context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CommentSent(Comment comment)
        {
            this.GetLogger().Info("User " + comment.Email + " sent a comment: " + comment.Content);

            return View();
        }
    }
}
