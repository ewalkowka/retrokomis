﻿using NUnit.Framework;
using RetroKomisMVC.Models;
namespace NUnitTestProject1
{
    [TestFixture(typeof(User))]
    public class AccommodateTests<AccommodatableUser> where AccommodatableUser : IAccommodatableUser, new()
    {
        IAccommodatableUser tested;

        [SetUp]
        public void Setup()
        {
            tested = new AccommodatableUser();
        }


        private static object[] accommodateTestCases = new object[]
        {
            new object[]
            {
                new AccommodatableUser { Login = "admin" },
                "admin"
            },
                new object[]
            {
                new AccommodatableUser{ Login = "user"},
                "user"
            },
                new object[]
            {
                new AccommodatableUser{ Login = "guest"},
                "guest"
            }
        };

        [TestCaseSource("accommodateTestCases")]
        public void AccommodateTest(AccommodatableUser reference, string user)
        {
            tested.AccommodateUser(user);

            Assert.AreEqual(reference.Login, tested.Login);
        }
    }
}
