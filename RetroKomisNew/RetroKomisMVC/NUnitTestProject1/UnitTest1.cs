using RetroKomisMVC.Models;
using NUnit.Framework;

namespace NUnitTestProject
{
    [TestFixture(typeof(Car))]
    public class AccommodateTests<Accommodatable> where Accommodatable : IAccommodatable, new()
    {
        IAccommodatable tested;

        [SetUp]
        public void Setup()
        {
            tested = new Accommodatable();
        }


        private static object[] accommodateTestCases = new object[]
        {
            new object[]
            {
                new Accommodatable{ Brand = "Audi" },
                "Audi"
            },
                new object[]
            {
                new Accommodatable{ Brand = "Fiat", Model = "126p", Price = 1223123, Milometer = 1000, Registration = "PO21364" },
                "Fiat"
            },
                new object[]
            {
                new Accommodatable{ Brand = "Ford", Model = "Mustang", Price = 123456, Milometer = 1203, Registration = "DE3004" },
                "Ford"
            }
        };

        [TestCaseSource("accommodateTestCases")]
        public void AccommodateTest(Accommodatable reference, string car)
        {
            tested.Accommodate(car);

            Assert.AreEqual(reference.Brand, tested.Brand);
        }
    }
}
